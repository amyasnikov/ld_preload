
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

char* (*orig_getenv)(const char *) = NULL;

char* getenv(const char *name) {
  if (!orig_getenv)
    orig_getenv = dlsym(RTLD_NEXT, "getenv");

  if (strcmp(name, "LD_PRELOAD") == 0)
    return NULL;

  return orig_getenv(name);
}

