
all:
	gcc -shared -fPIC ./ld_sum.c -o libsum.so
	gcc -shared -fPIC ./ld_sum_fake.c -o libsumfake.so
	gcc -shared -fPIC ./ld_rand.c -o librand.so
	gcc -shared -fPIC ./ld_getenv.c -o libgetenv.so -ldl
	gcc sample1.c -o sample1 -L. -lsum

clean:
	rm -f libsum.so
	rm -f libsumfake.so
	rm -f librand.so
	rm -f libgetenv.so
	rm -f sample1

test:
	LD_PRELOAD="./libsumfake.so ./libgetenv.so" LD_LIBRARY_PATH=. ./sample1

