
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sum2(int a, int b);
int sum3(int a, int b, int c);

int main(int argc, char *argv[], char *envp[]) {

  char* env = getenv("LD_PRELOAD");
  printf("env LD_PRELOAD: %s \n", env ? env : "(empty)");

  srand(time(NULL));

  {
    int a = rand() % 100;
    int b = rand() % 100;
    int r = sum2(a, b);
    printf("sum2(%d, %d) = %d \n", a, b, r);
  }

  {
    int a = rand() % 100;
    int b = rand() % 100;
    int c = rand() % 100;
    int r = sum3(a, b, c);
    printf("sum3(%d, %d, %d) = %d \n", a, b, c, r);
  }
}

